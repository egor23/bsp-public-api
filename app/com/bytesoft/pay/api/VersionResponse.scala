package com.bytesoft.pay.api

import play.api.libs.json.Json

case class VersionResponse(version: String)

object VersionResponse {
  implicit val j_VersionResponse = Json.format[VersionResponse]
}

