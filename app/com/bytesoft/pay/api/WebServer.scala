package com.bytesoft.pay.api

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpResponse}
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.typesafe.config._
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.Json

import scala.concurrent.ExecutionContextExecutor

object WebServer extends LazyLogging {
  def main(args: Array[String]) {

    implicit val system: ActorSystem = ActorSystem("my-system")
    implicit val materializer: ActorMaterializer = ActorMaterializer()
    implicit val executionContext: ExecutionContextExecutor = system.dispatcher

    val config = ConfigFactory.load()
    val address = config.getString("http-server.listen-address")
    val port = config.getInt("http-server.listen-port")
    val version = config.getString("app-info.version")

    val route =
      pathSingleSlash {
        get {
          complete(HttpResponse(entity = HttpEntity(ContentTypes.`application/json`, Json.toJson(VersionResponse(version)).toString)))
        }
      }

    Http().bindAndHandle(route, address, port)

    logger.info(s"\nServer online at http://$address:$port/")
  }


}
