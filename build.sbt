import sbt._

name := "Public-api"

version := "0.1"

scalaVersion := "2.12.7"

Compile/mainClass := Some("com.bytesoft.pay.api.WebServer")

libraryDependencies ++= Seq(
  "com.typesafe.akka"             %% "akka-http"              % "10.1.5",
  "com.typesafe.akka"             %% "akka-stream"            % "2.5.18",
  "com.typesafe.play"             %% "play-json"              % "2.6.9",
  "ch.qos.logback"                %  "logback-classic"        % "1.2.3",
  "ch.qos.logback"                %  "logback-core"           % "1.2.3",
  "com.typesafe.scala-logging"    %% "scala-logging"          % "3.9.0",
  "com.typesafe"                  % "config"                  % "1.3.3"
)